#include <GPIOObject.h>
#include <stm32f0xx.h>
#include <stm32f0xx_rcc.h>
#include <stm32f0xx_gpio.h>
#include <stm32f0xx_tim.h>

#ifdef __cplusplus
extern "C"
{
#endif

void EXTI0_1_IRQHandler(void);
void TIM3_IRQHandler(void);
void TIM14_IRQHandler(void);

#ifdef __cplusplus
}
#endif

bool volatile pin7_state = false;
bool volatile pin8_state = false;
GPIOObject *portC;

//timers
uint32_t upper_value = 5000;
uint32_t preciser = 1;
bool up = true;

uint32_t step = 0;

int main()
{
	portC = new GPIOObject(RCC_AHBPeriph_GPIOC);
	portC->initPin(GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9, GPIO_Mode_OUT);

//	portC->setPin(9, true);

	//interruptions
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	SYSCFG->EXTICR[0] &= (0x000F);
	EXTI->RTSR = EXTI_RTSR_TR0;
	EXTI->IMR = EXTI_IMR_MR0;
	NVIC_SetPriority(EXTI0_1_IRQn, 1);
	NVIC_EnableIRQ(EXTI0_1_IRQn);

	//timer
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3 | RCC_APB1Periph_TIM14, ENABLE);

	TIM_TimeBaseInitTypeDef base_timer;
	TIM_TimeBaseStructInit(&base_timer);
	base_timer.TIM_Prescaler = (uint16_t) (SystemCoreClock / 100000) - 1;
	base_timer.TIM_Period = 1;

	//init timer 3
	TIM_TimeBaseInit(TIM3, &base_timer);

	TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);

	TIM_Cmd(TIM3, ENABLE);

	NVIC_EnableIRQ(TIM3_IRQn);

	//init timer 14
	base_timer.TIM_Prescaler = (uint16_t) (SystemCoreClock / 1000) - 1;
	base_timer.TIM_Period = 1;
	TIM_TimeBaseInit(TIM14, &base_timer);

	TIM_ITConfig(TIM14, TIM_IT_Update, ENABLE);

	TIM_Cmd(TIM14, ENABLE);

	NVIC_EnableIRQ(TIM14_IRQn);

	while (1)
	{
	}

	portC->setPin(8, true);

	return 0;
}

void EXTI0_1_IRQHandler(void)
{
	if ((EXTI->IMR & EXTI_IMR_MR0) && (EXTI->PR & EXTI_PR_PR0))
	{
		EXTI->PR |= EXTI_PR_PR0;
	}

}

void TIM3_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)
	{
		TIM_ClearITPendingBit(TIM3, TIM_IT_Update);

		++step;
		if (step > preciser)
		{
			pin7_state = not pin7_state;
			portC->setPin(7, pin7_state);
			step = 0;
		}
	}
}

void TIM14_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM14, TIM_IT_Update) != RESET)
	{

		TIM_ClearITPendingBit(TIM14, TIM_IT_Update);

		pin8_state = not pin8_state;
		portC->setPin(8, pin8_state);

		if (up)
		{
			++preciser;

			if (preciser >= upper_value)
			{
				up = false;
			}
		}
		else
		{
			--preciser;
			if (preciser <= 1)
			{
				up = true;
			}
		}
	}
}
