/*
 * GPIO.h
 *
 *  Created on: May 21, 2015
 *      Author: Denis Lobanov
 */

#ifndef GPIOOBJECT_H_
#define GPIOOBJECT_H_

#include <stm32f0xx_gpio.h>
#include <stm32f0xx_rcc.h>

class GPIOObject
{
public:
	GPIOObject(uint32_t port);
	virtual ~GPIOObject();
	void initPin(uint16_t pin, GPIOMode_TypeDef type);
	void setPin(uint16_t pin, bool state);


	uint32_t port;
	GPIO_TypeDef *gpio;
	GPIO_InitTypeDef defaultSettingsIn, defaultSettingsOut, *currentStruct;
};

#endif /* GPIOOBJECT_H_ */
